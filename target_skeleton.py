# Skeleton RPython Program
# ========================
#
# This will work with optimiser flag up to -O2. If you want -Ojit
# then you must add the JIT merge point.

import sys

# Your actual functionality will be here
def main(args):
    print("Hello RPython World!")
    print("Args: %s" % args)
    return 0 # Needs to return an int

# RPython starts building a flowchart here
def target(*args):
    return main, None

# The default JitPolicy usually suffices
def jitpolicy(driver):
    from rpython.jit.codewriter.policy import JitPolicy
    return JitPolicy()

# For running untranslated
if __name__ == "__main__":
    main(sys.argv)
